<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('location_id');
            $table->integer('distributor_id');
            $table->decimal('gypsum_premium_97', 10, 2)->nullable();
            $table->decimal('sop_ultra_fines', 10, 2)->nullable();
            $table->decimal('kms', 10, 2)->nullable();
            $table->decimal('dryer_fines', 10, 2)->nullable();
            $table->decimal('terra_alba', 10, 2)->nullable();
            $table->decimal('turf_and_ornamental', 10, 2)->nullable();
            $table->decimal('spread_bulk', 10, 2)->nullable();
            $table->decimal('spread_2000lb', 10, 2)->nullable();
            $table->decimal('spread_quarry', 10, 2)->nullable();
            $table->decimal('aquadrive_265', 10, 2)->nullable();
            $table->decimal('aquadrive_2x2', 10, 2)->nullable();
            $table->decimal('machine_175', 10, 2)->nullable();
            $table->decimal('machine_325', 10, 2)->nullable();
            $table->decimal('machine_525', 10, 2)->nullable();
            $table->decimal('feed_grade', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
