<?php

namespace App\Nova;

//use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\BelongsTo;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Price extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Price';
    public static $defaultSort = 'location.full_city';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [];


    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {

        // Get the location's name
        $query->join('locations', 'prices.location_id', '=', 'locations.id');

        if (empty($request->get('orderBy')) || $request->get('orderBy') == null || $request->get('orderBy') == 'location_id') {
            $query->getQuery()->orders = [];
            $query->orderBy('locations.full_city');
        }

        $query->select('prices.*', 'locations.full_city');
        return $query;

    }


    protected static function applyOrderings($query, array $orderings)
    {
        if (empty($orderings)) {
            // This is your default order
            return $query->orderBy('locations.full_city', 'asc');
        }

        foreach ($orderings as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            //ID::make()->sortable(),
            BelongsTo::make('Distributor')->searchable()->sortable()->required()->help(
              '<a
                  target="popup"
                  href="/resources/distributors/new"
                  onclick="window.open(\'/resources/distributors/new\',\'popup\',\'width=600,height=400\'); return false;"
              >
              Create new distributor</a>'
            ),
            BelongsTo::make('Location')->searchable()->sortable()->required()->help(
              '<a
                  target="popup"
                  href="/resources/locations/new"
                  onclick="window.open(\'/resources/locations/new\',\'popup\',\'width=600,height=400\'); return false;"
              >
              Create new location</a>'
            ),
            Text::make('Bag Rate', function () { return $this->ComputedBagRate; })->sortable()->readonly(),
            Text::make('Bulk Rate', function () { return $this->ComputedBulkRate; })->sortable()->readonly(),
            Currency::make('Gypsum Premium 97')->sortable(),
            Currency::make('Sop Ultra Fines')->sortable(),
            Currency::make('KMS')->sortable(),
            Currency::make('Dryer Fines')->sortable(),
            Currency::make('Terra Alba')->sortable(),
            Currency::make('Turf and Ornamental')->sortable(),
            Currency::make('Spread Bulk')->sortable(),
            Currency::make('Spread 2000lb')->sortable(),
            Currency::make('Spread Quarry')->sortable(),
            Currency::make('Aquadrive 265')->sortable(),
            Currency::make('Aquadrive 2x2')->sortable(),
            Currency::make('Machine 175')->sortable(),
            Currency::make('Machine 325')->sortable(),
            Currency::make('Machine 525')->sortable(),
            Currency::make('Feed Grade')->sortable(),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
      return [
          (new DownloadExcel)->withHeadings(),
      ];
    }
}
