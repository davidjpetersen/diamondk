<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
  protected $fillable = [
    'city',
    'state',
    'county',
    'full_city',
    'bag_rate',
    'bulk_rate',
  ];


  public function prices()
  {
      return $this->hasMany('App\Price');
  }

  public function region()
  {
      return $this->belongsTo('App\Region');
  }


}
