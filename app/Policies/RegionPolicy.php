<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
      return true;
    }

    public function create(User $user)
    {
      return $user->admin;
    }

    public function update(User $user)
    {
      return $user->admin;
    }

    public function delete(User $user)
    {
      return $user->admin;
    }
}
