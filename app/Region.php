<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{


  public static $defaultSort = 'name';
  protected $fillable = [
      'name',
  ];

  public function locations()
  {
      return $this->hasMany('App\Location');
  }

}
