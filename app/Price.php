<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
  protected $fillable = [
    'location_id',
    'distributor_id',
    'gypsum_premium_97',
    'sop_ultra_fines',
    'kms',
    'dryer_fines',
    'terra_alba',
    'turf_and_ornamental',
    'spread_bulk',
    'spread_2000lb',
    'spread_quarry',
    'aquadrive_265',
    'aquadrive_2x2',
    'machine_175',
    'machine_325',
    'machine_525',
    'feed_grade',
  ];

  protected $appends = ['ComputedBagRate','ComputedBulkRate'];

  public function distributor()
  {
      return $this->belongsTo('App\Distributor');
  }

  public function location()
  {
      return $this->belongsTo('App\Location');
  }

  public function getSurcharge() {
    return floatval(DB::table('surcharge')->get()->first()->shipping_surcharge);
  }

  public function getComputedBagRateAttribute() {
      if(!$this->id) { return 0; }
      $location = $this->location()->first();
      if(isset($location->bag_rate)) {
        setlocale(LC_MONETARY, 'en_US');
        return money_format('%(#10n', $this->location()->first()->bag_rate * $this->getSurcharge() / 100);
      } else {
        return 0;
      }

  }

  public function getComputedBulkRateAttribute() {
      if(!$this->id) { return 0; }
      $location = $this->location()->first();
      if(isset($location->bulk_rate)) {
        setlocale(LC_MONETARY, 'en_US');
        return money_format('%(#10n', $this->location()->first()->bulk_rate * $this->getSurcharge() /100);
      } else {
        return 0;
      }
  }

}
