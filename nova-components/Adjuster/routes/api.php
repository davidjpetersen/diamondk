<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::get('/getSurcharge', '\Diamondk\Adjuster\Http\Controllers\SettingsController@getSurcharge');
Route::put('/setSurcharge', '\Diamondk\Adjuster\Http\Controllers\SettingsController@setSurcharge');

Route::get('/getDistributors', '\Diamondk\Adjuster\Http\Controllers\SettingsController@getDistributors');

Route::put('/adjustRates', '\Diamondk\Adjuster\Http\Controllers\SettingsController@adjustRates');
Route::put('/adjustPrices', '\Diamondk\Adjuster\Http\Controllers\SettingsController@adjustPrices');
