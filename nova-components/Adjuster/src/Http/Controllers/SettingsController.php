<?php

namespace Diamondk\Adjuster\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Distributor;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function getSurcharge () {
    return DB::table('surcharge')->get()->first()->shipping_surcharge;
  }

  public function getDistributors () {
    return Distributor::all();
  }

  public function setSurcharge (Request $request) {
    DB::table('surcharge')
      ->where('id', 1)
      ->update(['shipping_surcharge' => $request->input('shipping_surcharge')]);

    return $this->getSurcharge();
  }

  public function adjustPrices (Request $request) {

    $price = $request->input('price');

      if (empty($price['amount'])) { return 'true'; }
      if (empty($price['product']) || empty($price['distributor']) || empty($price['state']) ) { abort(422); }

      $location = Arr::pluck(
        json_decode(json_encode(
            DB::table('locations')
            ->where('state', $price['state'])
            ->select('id')
            ->get()
        , true)),'id');

        $query = DB::table('prices');
        if($price['distributor'] != '*') $query->where('distributor_id', $price['distributor']);
            if($price['state'] != '*') $query->whereIn('location_id', $location);

        $products = array(
          'gypsum_premium_97',
          'sop_ultra_fines',
          'kms',
          'dryer_fines',
          'terra_alba',
          'turf_and_ornamental',
          'spread_bulk',
          'spread_2000lb',
          'spread_quarry',
          'aquadrive_265',
          'aquadrive_2x2',
          'machine_175',
          'machine_325',
          'machine_525',
          'feed_grade'
        );

        $selectedProduct = $price['product'];

        foreach ($products as $product) {
          if ($selectedProduct == '*' || $selectedProduct == $product) {
            $query->increment($product, $price['amount']);
          }
        }

    return 'true';

  }

  public function adjustRates (Request $request) {

    $rate = $request->input('rate');
    if (empty($rate['amount'])) { abort(422); }

    $location = Arr::pluck(
      json_decode(json_encode(
          DB::table('locations')
          ->where('state', $rate['state'])
          ->select('id')
          ->get()
      , true)),'id');

    $query = DB::table('locations');
    if($rate['state'] != '*') $query->whereIn('location_id', $location);
    $query->increment($rate['type'], $rate['amount']);
    $query->dd();
    return $query->toSql();
  }


}
